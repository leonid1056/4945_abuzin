**Задание на лабораторную работу:**
написать простой unit-файл.

**Выполнение:**
Unit-файл после запуска будет выводить строку “hello world” каждые 10 секунд. Перед завершением выводится строка “bye!”.

Текст скрипта `/home/liveuser/script.sh`:
```
#!/bin/bash
while $(sleep 30);
do
    echo "hello world"
done
```
![](OS_Linux/lab3/images/1.png)

Текст скрипта `/home/liveuser/script2.sh`:
```
#!/bin/bash
echo "Bye!"
```
![](OS_Linux/lab3/images/2.png)

Нужно сделать их исполняемыми:
```
sudo chmod a+x script.sh
sudo chmod a+x script2.sh
```

Текст Unit-файла `/etc/systemd/system/hello.service`:
```
[Unit]
Description=hello

[Service]
Type=simple
ExecStart=/home/liveuser/script.sh
ExecStop=/home/liveuser/script2.sh
```
![](OS_Linux/lab3/images/3.png)

Запуск сервиса. Проверка статуса. Завершение. Проверка статуса.

![](OS_Linux/lab3/images/4.png)

Вывод логов командой
```
journalctl -u hello-world -e
```
![](OS_Linux/lab3/images/5.png)

**Вывод:** Systemd обеспечивает асинхронную инициализацию системных сервисов во время загрузки, активацию сервисов по требованию, управление сервисами, на основе зависимостей, удаленное управление сервисами. Systemd является более современным решением по сравнению с системой инициализации sysvinit, с которой она обратно совместима.
